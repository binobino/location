package com.bino;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by bino on 17.02.17 г..
 */
public class Hello {

    private static Locations locations = new Locations();

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        locations.put(0, new Location(0, "You are sitting in front of the Computer",null));


        Map<String,String> vocabilary = new HashMap<>();
        vocabilary.put("NORTH","N");
        vocabilary.put("EAST","E");
        vocabilary.put("SOUTH","S");
        vocabilary.put("WEST","W");
        vocabilary.put("QUIT","Q");
        vocabilary.put("N","N");
        vocabilary.put("E","E");
        vocabilary.put("S","S");
        vocabilary.put("W","W");
        vocabilary.put("Q","Q");

        int loc = 1;
        while (true) {
            System.out.println(locations.get(loc).getDescription());
            if (loc == 0){
                break;
            }

            Map<String,Integer> exits = locations.get(loc).getExits();
            System.out.print("Available exists are ");
            for (String exit:exits.keySet()){
                System.out.print(exit + ", ");
            }
            System.out.println();
            String direction = scanner.nextLine().toUpperCase();
            String[] words = direction.split(" ");
            for (String word : words) {
                if (vocabilary.containsKey(word)) {
                    direction = vocabilary.get(word);
                    break;
                }
            }
            if (exits.containsKey(direction)){
                loc = exits.get(direction);
            } else {
                System.out.println("You cannot go in that direction !");
            }
        }


    }

}
